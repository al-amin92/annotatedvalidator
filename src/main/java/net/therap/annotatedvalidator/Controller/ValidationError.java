package net.therap.annotatedvalidator.Controller;

/**
 * @author al-amin
 * @since 11/8/16
 */
public class ValidationError {
    private String errorMessage;

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessege() {
        return errorMessage;
    }

}
