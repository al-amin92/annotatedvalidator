package net.therap.annotatedvalidator.Controller;

import net.therap.annotatedvalidator.Annotation.Size;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 11/8/16
 */
public class AnnotatedValidator {

    public static List<ValidationError> validate(Object obj) throws IllegalAccessException, NoSuchFieldException {
        List<ValidationError> errorList = new ArrayList<>();
        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field field : fields) {
            if (!field.isAnnotationPresent(Size.class)) {
                continue;
            }
            field.setAccessible(true);
            Size annotation = field.getAnnotation(Size.class);

            String fieldName = field.getName();
            String fieldType = field.getType().getSimpleName();
            String errorMessage = annotation.message();

            int min = annotation.min();
            int max = annotation.max();

            if (fieldType.equals("String")) {
                int length = ((String) field.get(obj)).length();
                if (length < min || length > max) {
                    errorList.add(getErrorInfo(errorMessage, fieldName, fieldType, min, max));
                }
            } else if (fieldType.equals("int")) {
                int age = (int) field.get(obj);
                if (age < min) {
                    errorList.add(getErrorInfo(errorMessage, fieldName, fieldType, min, max));
                }
            }
        }

        return errorList;
    }

    public static ValidationError getErrorInfo(String message, String fieldName, String fieldType, int min, int max) {
        ValidationError error = new ValidationError();
        message = message.replaceAll("\\{min\\}", Integer.toString(min));
        message = message.replaceAll("\\{max\\}", Integer.toString(max));

        error.setErrorMessage(fieldName + "(" + fieldType + "): " + message);

        return error;
    }

    public static void print(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            System.out.println(error.getErrorMessege());
        }
        System.out.println();
    }
}
