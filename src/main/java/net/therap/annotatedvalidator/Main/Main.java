package net.therap.annotatedvalidator.Main;

import net.therap.annotatedvalidator.Controller.AnnotatedValidator;
import net.therap.annotatedvalidator.Controller.ValidationError;
import net.therap.annotatedvalidator.Objects.Person;

import java.util.List;

/**
 * @author al-amin
 * @since 11/8/16
 */
public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Person person = new Person("Abcde Fghijk", 5);
        System.out.println("Name: " + person.getName() + ", Age: " + person.getAge());
        List<ValidationError> errors = AnnotatedValidator.validate(person);
        AnnotatedValidator.print(errors);

        person.setName("TherapBD");
        person.setAge(10);
        System.out.println("Name: " + person.getName() + ", Age: " + person.getAge());
        errors = AnnotatedValidator.validate(person);
        AnnotatedValidator.print(errors);

        person.setName("qwertyuiopasdfgf");
        person.setAge(19);
        System.out.println("Name: " + person.getName() + ", Age: " + person.getAge());
        errors = AnnotatedValidator.validate(person);
        AnnotatedValidator.print(errors);

        person.setName("Therap");
        person.setAge(77);
        System.out.println("Name: " + person.getName() + ", Age: " + person.getAge());
        errors = AnnotatedValidator.validate(person);
        AnnotatedValidator.print(errors);
    }
}
