package net.therap.annotatedvalidator.Objects;

import net.therap.annotatedvalidator.Annotation.Size;

/**
 * @author al-amin
 * @since 11/8/16
 */
public class Person {
    @Size(max = 10)
    private String name;
    @Size(min = 18, message = "Age can not be less than {min}")
    private int age;

    private int age1;

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }
}

